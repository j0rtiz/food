# Food

API para cadastro de produtos e realização/controle de pedidos

## Pré-requisitos

* NodeJS
* MySQL

## Usuario MySQL

* username: root
* password: root

## Execução do projeto:

1. Clonar o projeto
2. Acessar a pasta do projeto e instalar as dependências "npm i"
3. Criar banco de dados chamado "food" no MySQL
4. Executar os migrations para criação das tabelas "npm run migration"
5. Executar o projeto "npm start"

## Rotas

### Produtos

* POST /products - cria novo produto 
* GET /products - lista todos os produtos
* GET /products/:id - lista produto por ID
* PUT /products/:id - atualiza produto por ID
* DELETE /products/:id - remove produto por ID
* GET /catalog - lista produtos que estão ativos

### Usuários

* POST /users - cria novo usuário, juntamente com seu carrinho de compras 
* GET /users - lista todos os usuários
* GET /users/:id - lista usuário por ID
* PUT /users/:id - atualiza usuário por ID
* DELETE /users/:id - remove usuário por ID

### Carrinhos

* POST /users/:user_id/carts - adiciona produto no carrinho, por usuário
* GET /users/:user_id/carts - lista carrinho por usuário ID
* DELETE /products/:id - limpa carrinho por usuário ID

Exemplo de payload para adição de produto no carrinho:
```json
{
    productId: 1,
    amount: 5
}
```

## Principais Libs utilizadas

* Express
* Sequelize
* mysql2
* eslint
