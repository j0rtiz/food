const { Model, DataTypes } = require('sequelize');

class User extends Model {
  static init(sequelize) {
    super.init({
      nome: DataTypes.STRING,
      email: DataTypes.STRING,
      telefone: DataTypes.STRING,
      senha: DataTypes.STRING,
    }, {
      sequelize,
      timestamps: false,
    });
  }

  static associate(models) {
    this.hasOne(models.Cart, { foreignKey: 'user_id', as: 'cart' });
  }
}

module.exports = User;
