const { Model, DataTypes } = require('sequelize');

class Product extends Model {
  static init(sequelize) {
    super.init({
      nome: DataTypes.STRING,
      descricao: DataTypes.TEXT,
      preco: DataTypes.DECIMAL(10, 2),
      imagem: DataTypes.TEXT,
      tags: DataTypes.JSON,
      status: DataTypes.ENUM('ativo', 'inativo'),
    }, {
      sequelize,
      timestamps: false,
    });
  }
}

module.exports = Product;
