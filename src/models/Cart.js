const { Model, DataTypes } = require('sequelize');

class Cart extends Model {
  static init(sequelize) {
    super.init({
      products: {
        type: DataTypes.TEXT,
        defaultValue: '[]',
        get() {
          return JSON.parse(this.getDataValue('products'));
        },
        set(val) {
          return this.setDataValue('products', JSON.stringify(val));
        },
      },
    }, {
      sequelize,
    });
  }

  static associate(models) {
    this.belongsTo(models.User, { foreignKey: 'user_id', as: 'user' });
  }
}

module.exports = Cart;
