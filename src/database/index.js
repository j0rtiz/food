const Sequelize = require('sequelize');
const dbConfig = require('../config/database');

const connection = new Sequelize(dbConfig);

const Product = require('../models/Product');
const Cart = require('../models/Cart');
const User = require('../models/User');

Product.init(connection);
User.init(connection);
Cart.init(connection);

User.associate(connection.models);
Cart.associate(connection.models);

module.exports = connection;
