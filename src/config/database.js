module.exports = {
  dialect: 'mysql',
  host: 'localhost',
  username: 'root',
  password: 'root',
  database: 'food',
  define: {
    timestamps: true,
    underscored: true,
  },
};
