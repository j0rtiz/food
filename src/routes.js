const express = require('express');

const routes = express.Router();

const ProductController = require('./controllers/ProductController');
const UserController = require('./controllers/UserController');
const CartController = require('./controllers/CartController');

routes.post('/products', ProductController.createProduct);
routes.get('/products', ProductController.getAllProducts);
routes.get('/products/:id', ProductController.getProductById);
routes.put('/products/:id', ProductController.updateProduct);
routes.delete('/products/:id', ProductController.deleteProduct);
routes.get('/catalog', ProductController.getAllActiveProducts);

routes.post('/users', UserController.createUser);
routes.get('/users', UserController.getAllUsers);
routes.get('/users/:id', UserController.getUserById);
routes.put('/users/:id', UserController.updateUser);
routes.delete('/users/:id', UserController.deleteUser);

routes.post('/users/:user_id/carts', CartController.addToCart);
routes.get('/users/:user_id/carts', CartController.getCartById);
routes.delete('/users/:user_id/carts', CartController.deleteFromCart);

module.exports = routes;
