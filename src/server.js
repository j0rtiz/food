const express = require('express');
const routes = require('./routes');

require('./database');

const app = express();
const port = 3333;

app.use(express.json());
app.use(routes);

app.listen(port, () => {
  console.clear();
  console.log(`Web server listening at: http://localhost:${port}`);
});
