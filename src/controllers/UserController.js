const User = require('../models/User');
const Cart = require('../models/Cart');

module.exports = {
  async createUser(req, res) {
    try {
      const user = await User.create(req.body);

      await Cart.create({ user_id: user.id });

      return res.status(200).json(user);
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async getAllUsers(req, res) {
    try {
      const users = await User.findAll();

      return res.status(200).json(users);
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async getUserById(req, res) {
    try {
      const user = await User.findOne({ where: { id: req.params.id } });

      if (user) {
        return res.status(200).json(user);
      }

      return res.status(404).send({ error: 'Usuário não existe' });
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async updateUser(req, res) {
    try {
      const [updated] = await User.update(req.body, { where: { id: req.params.id } });

      if (updated) {
        const user = await User.findOne({ where: { id: req.params.id } });

        return res.status(200).json(user);
      }

      throw new Error('Usuário não existe');
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async deleteUser(req, res) {
    try {
      const count = await User.destroy({ where: { id: req.params.id } });

      if (count) {
        return res.status(200).send({ count });
      }

      throw new Error('Usuário não existe');
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
};
