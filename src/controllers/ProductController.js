const Product = require('../models/Product');

module.exports = {
  async createProduct(req, res) {
    try {
      const product = await Product.create(req.body);

      return res.status(200).json(product);
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async getAllProducts(req, res) {
    try {
      const products = await Product.findAll();

      return res.status(200).json(products);
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async getProductById(req, res) {
    try {
      const product = await Product.findOne({ where: { id: req.params.id } });

      if (product) {
        return res.status(200).json(product);
      }

      return res.status(404).send({ error: 'Produto não existe' });
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async updateProduct(req, res) {
    try {
      const [updated] = await Product.update(req.body, { where: { id: req.params.id } });

      if (updated) {
        const product = await Product.findOne({ where: { id: req.params.id } });

        return res.status(200).json(product);
      }

      throw new Error('Produto não existe');
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async deleteProduct(req, res) {
    try {
      const count = await Product.destroy({ where: { id: req.params.id } });

      if (count) {
        return res.status(200).send({ count });
      }

      throw new Error('Produto não existe');
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async getAllActiveProducts(req, res) {
    try {
      const products = await Product.findAll({ where: { status: 'ativo' } });

      return res.status(200).json(products);
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
};
