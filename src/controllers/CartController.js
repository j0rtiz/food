const User = require('../models/User');
const Cart = require('../models/Cart');

module.exports = {
  async addToCart(req, res) {
    try {
      const { user_id } = req.params;
      const { productId, amount } = req.body;
      const cart = await Cart.findOne({ where: { user_id } });
      const { products } = cart;

      if (!cart) {
        return res.status(400).json({ error: 'Carrinho não existe' });
      }

      if (!productId || !amount) {
        return res.status(400).json({ error: 'ID do produto e/ou quantidade não foi informado' });
      }

      products.push({ productId, amount });
      cart.products = products;
      await cart.save();

      return res.status(200).json({ cart_id: cart.id });
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async getCartById(req, res) {
    try {
      const { user_id } = req.params;
      const user = await User.findByPk(user_id, { include: { association: 'cart' } });

      if (!user) {
        return res.status(400).send({ error: 'Usuário não existe' });
      }

      return res.status(200).json(user);
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
  async deleteFromCart(req, res) {
    try {
      const { user_id } = req.params;
      const cart = await Cart.findOne({ where: { user_id } });

      if (!cart) {
        return res.status(400).json({ error: 'Carrinho não existe' });
      }

      cart.products = [];
      await cart.save();

      return res.status(200).json({ cart_id: cart.id });
    } catch (error) {
      return res.status(500).send({ error: error.message });
    }
  },
};
